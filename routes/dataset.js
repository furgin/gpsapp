module.exports = function() {

    console.log("initializing dataset")

    const EventEmitter = require('events');
    const util = require('util');

    function MyEmitter() {
        EventEmitter.call(this);
    }
    util.inherits(MyEmitter, EventEmitter);

    var myEmitter = new MyEmitter();

    var bcrypt = require('bcrypt');
    var salt = "$2a$10$UogFXQSfszz3LHdBY.kdvu";
    console.log(salt)

    var mongoose = require('mongoose');
    var async = require('async');
    mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/gpsapp');

    var userSchema = mongoose.Schema({
        email: String,
        password: String
    });

    var playerSchema = mongoose.Schema({
        dci: String,
        first: String,
        last: String
    })

    var playerRoundSchema = mongoose.Schema({
        dci: String,
        roundId: String,
        seatNumber: String
    })

    var roundSchema = mongoose.Schema({
        eventId: String,
        number: String
    })

    var eventSchema = mongoose.Schema({
        eventId: String,
        name: String,
        location: String,
        active: {
            type: Boolean,
            default: false
        },
        rounds: [{
            number: String,
            tables: [{
                number: String,
                seats: [{
                    number: String,
                    player: String
                }]
            }]
        }],
        players: [{
            dci: String,
            name: String
        }]
    })

    var Player = mongoose.model('Player', playerSchema);
    var Round = mongoose.model('Round', roundSchema);
    var PlayerRound = mongoose.model('PlayerRound', playerRoundSchema);
    var User = mongoose.model('User', userSchema);
    var Event = mongoose.model('Event', eventSchema)

    var _updatePlayer = function(dci, player, callback) {
        Player.findOne({
            dci: dci
        }, function(err, result) {
            if (result != null) {
                for (var key in player) {
                    if (player.hasOwnProperty(key)) {
                        result[key] = player[key];
                    }
                }
                result.save(function(err, result) {
                    callback(err, result);
                });
            }
        });
    }

    var _createPlayer = function(player, callback) {
        var newPlayer = new Player(player);
        newPlayer.save(callback);
    };

    var _getPlayer = function(dci, callback) {
        Player.findOne({
            dci: dci
        }, function(err, player) {
            callback(err, player);
        })
    };

    var _getRounds = function(dci, callback) {
        PlayerRound.find({
            dci: dci
        }).exec(function(array) {
            // TODO add admin section 
        });
    }

    var _getRoundByEvent = function(eventId, number, callback) {
        Round.find({
            eventId: eventId,
            number: number
        }, function(err, result) {
            callback(err, result);
        })
    }

    var _createRound = function(round, callback) {
        var newRound = new Round(round);
        newRound.save(function(err, result) {
            callback(err, result);
        })
    }

    var _getUser = function(email, password, callback) {

        User.find({}, function(err, all) {
            if (all === null || all.length === 0) {
                console.log('***** add the admin user');
                // add the admin user
                var admin = new User({
                    email: "admin",
                    password: bcrypt.hashSync("admin", salt)
                });
                admin.save(new function(err) {
                    console.log("saved: " + admin)
                    callback(err, admin);
                });
            }
            else {
                var hash = bcrypt.hashSync(password, salt);
                // callback(err,user);
                User.findOne({
                    email: email,
                    password: hash
                }, function(err, user) {
                    console.log("found user: " + user + " " + err)
                    callback(err, user);
                })
            }
        })

    }

    var _updateEvent = function(eventId, event, callback) {
        Event.findOne({
            eventId: eventId
        }, function(err, result) {
            if (result != null) {
                for (var key in event) {
                    if (event.hasOwnProperty(key)) {
                        result[key] = event[key];
                    }
                }
                result.save(function(err, result) {
                    myEmitter.emit('event_created', result);
                    callback(err, result);
                });
            }
        });
    }

    var _createEvent = function(event, callback) {
        var newEvent = new Event(event);
        newEvent.save(function(err, result) {
            myEmitter.emit('event_created', result);
            callback(err, result);
        });
    }

    var _getEvents = function(opts,callback) {
        Event.find(opts||{}, function(err, all) {
            callback(err, all);
        })
    }

    var _getEvent = function(eventId, callback) {
        Event.findOne({
            eventId: eventId
        }, function(err, event) {
            callback(err, event);
        });
    }

    var _deleteEvent = function(eventId, callback) {
        Event.findOne({
            eventId: eventId
        }, function(err, model) {
            if (model) {
                model.remove(function(err) {
                    myEmitter.emit('event_deleted', model);
                    callback(err, model);
                });
            }
        });
    }

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("mongodb connection open")
    })

    myEmitter.createPlayer = _createPlayer;
    myEmitter.updatePlayer = _updatePlayer;
    myEmitter.getPlayer = _getPlayer;

    myEmitter.getRounds = _getRounds;
    myEmitter.getRoundByEvent = _getRoundByEvent;
    myEmitter.createRound = _createRound;

    myEmitter.getUser = _getUser;

    myEmitter.getEvents = _getEvents;
    myEmitter.getEvent = _getEvent;
    myEmitter.createEvent = _createEvent;
    myEmitter.updateEvent = _updateEvent;
    myEmitter.deleteEvent = _deleteEvent;

    return myEmitter;
};
