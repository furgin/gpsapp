module.exports = function() {

    var modes = {
        'player': {
            name: 'player'
        },
        'admin': {
            name: 'admin'
        },
        'unknown': {
            name: 'unknown'
        }
    }

    var sessions = {} // session ID -> {dci,admin}

    var _touch = function(sessionId) {
        if (typeof sessions[sessionId] != 'undefined') {
            console.log("[" + sessionId + "]: touched");
            sessions[sessionId].lastModified = new Date();
        }
    }

    var _createSession = function(sessionId, mode) {
        return sessions[sessionId] = {
            age: new Date(),
            mode: mode,
            sockets: [],
            resend: {}
        }
    }

    var _getSession = function(sessionId) {
        var session = sessions[sessionId];
        if (typeof session != 'undefined') {
            var age = (new Date()) - session.lastModified;
            if (age > 5 * 60 * 1000) { // 15 minutes
                console.log("[" + sessionId + "]: expire session (" + age + ")");
                // expire session
                delete sessions[sessionId];
            }
        }
        return sessions[sessionId];
    }

    var _attachSession = function(sessionId, socket, mode, callback) {
        if (typeof sessionId == 'undefined') {
            console.log("found no session");
            socket.emit('reload');
        }
        else {
            var session = _getSession(sessionId);
            var sockets = session.sockets;
            sockets.push(socket);

            console.log("[" + sessionId + "]: mode: " + mode);
            var handled = false;
            switch (mode) {
                case modes.unknown:
                case modes.player:
                    if (typeof session.dci == 'undefined') {
                        console.log("[" + sessionId + "][" + mode.name + "]: connected");
                        _login(sessionId);
                        handled = true;
                    }
                    break;
                case modes.admin:
                    if (typeof session.admin == 'undefined') {
                        console.log("[" + sessionId + "][" + mode.name + "]: connected");
                        _admin(sessionId);
                        handled = true;
                    }
            }
            if (!handled) {
                if (typeof session.resend[mode] != 'undefined') {
                    _resend(sessionId, mode, callback);
                }
                else {
                    callback(sessionId);
                }
            }
        }
    }

    var _resend = function(sessionId, mode, nohistory) {
        var resend = sessions[sessionId].resend[mode];
        if (typeof resend == 'undefined') {
            console.log("[" + sessionId + "]: no history for mode: "+mode);
            nohistory(sessionId);
        }
        else {
            console.log("[" + sessionId + "]: resend: " + resend.name);
            _page(sessionId, mode, resend.name, resend.data);
        }
    }

    // client pages

    var _waiting = function(sessionId) {
        _page(sessionId, 'player', 'page-noevent');
    }

    var _seating = function(sessionId, data) {
        _page(sessionId, 'player', 'page-seating', data);
    }

    var _login = function(sessionId) {
        _page(sessionId, 'player', 'page-login');
    }

    // admin pages

    var _admin = function(sessionId) {
        _page(sessionId, 'admin', 'page-admin-login');
    }

    var _adminEvents = function(sessionId, events) {
        _page(sessionId, 'admin', 'page-events', {
            events: events
        });
    }
    
    var _fileUploaded = function(sessionId, message) {
        _page(sessionId, 'admin', 'file-uploaded', {
            message: message
        });
    }

    var _page = function(sessionId, mode, name, data) {
        //console.log("[" + sessionId + "]["+mode+"]: " + name + (data ? " data: " + JSON.stringify(data) : ""));
        console.log("[" + sessionId + "]["+mode+"]: " + name);
        sessions[sessionId].resend[mode] = {
            name: name,
            data: data
        };

        var sockets = sessions[sessionId].sockets;
        var found = false;
        for (var index in sockets) {
            found = true;
            console.log("[" + sessionId + "]: socket: " + sockets[index].id);
            sockets[index].emit(name, data);
        }
        if (found) {
            _touch(sessionId);
        }
    }

    var _find = function(dci) {
        var found = sessions.filter(function(s) {
            return s.dci == dci;
        })
        console.log("[" + dci + "]: found: " + found.length);
        return found;
    }

    var _getSessions = function(dci, callback) {
        for (var sessionId in sessions) {
            if (sessions.hasOwnProperty(sessionId)) {
                if (sessions[sessionId].dci == dci) {
                    callback(sessionId, sessions[sessionId]);
                }
            }
        }
    }

    var _disconnect = function(sessionId, socketId) {
        var session = _getSession(sessionId);
        if (session) {
            session.sockets = session.sockets.filter(function(s) {
                return s.id != socketId;
            })
            console.log("[" + sessionId + ']: sockets: ' + session.sockets.length);
        }
    }

    return {
        touch: _touch,
        createSession: _createSession,
        getSession: _getSession,
        getSessions: _getSessions,
        attachSession: _attachSession,

        disconnect: _disconnect,

        admin: _admin,
        adminEvents: _adminEvents,
        fileUploaded: _fileUploaded,

        login: _login,
        waiting: _waiting,
        seating: _seating,
        resend: _resend,
        page: _page
    }
}
