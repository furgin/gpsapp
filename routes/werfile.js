module.exports = function() {

    var xml2js = require('xml2js');
    var async = require('async');

    var _load = function(text, callback) {
        xml2js.parseString(text, function(err, result) {
            
            var _log = function() {
                    return result["event"]["log"][0]["entry"].map(function(a) {
                        return {
                            "date": new Date(Date.parse(a["$"]["date"])),
                            "operation": a["$"]["operation"],
                            "text": a["_"]
                        }
                    }).sort(function(a, b) {
                        return a.date - b.date;
                    })
                };
            
            callback({
                event: function() {
                    return {
                        eventId: result["event"]["$"].eventguid,
                        name: result["event"]["$"].title,
                        location: result["event"]["parentevent"] ? result["event"]["parentevent"][0]["$"]["playlocation"] : ""
                    }
                },
                log: _log,
                round: function() {
                    return _log().filter(function(a) {
                        return a.operation === "ROUND";
                    }).length;
                },
                tables: function() {
                    var rawTables = result["event"]["seats"][0]["table"];
                    return rawTables.map(function(a){
                        var seats = a["seat"].map(function(b) {
                            return {
                                "number": b["$"]["number"],
                                "player": b["$"]["player"]
                            }
                        });
                        return {
                            "number": a["$"]["number"],
                            "seats": seats
                        } 
                    });
                },
                players: function() {
                    var persons = result["event"]["participation"][0]["person"];
                    return persons.map(function(a){
                        return {
                            dci: a["$"]["id"],
                            first: a["$"]["first"],
                            last: a["$"]["last"]
                        }
                    });
                }
            })
        });
    }

    return {
        load: _load,
    }
}