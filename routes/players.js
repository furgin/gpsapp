module.exports = function(io, dataset, sessions) {

    var async = require('async');

    var notifyRound = function(event, opts) {

        var process = function(round) {
            var roundName = "Round " + round.number;
            for (var j = 0, lenj = round.tables.length; j < lenj; j++) {
                var table = round.tables[j];
                (function(table) {
                    for (var k = 0, lenk = table.seats.length; k < lenk; k++) {
                        var seat = table.seats[k];
                        // $("#opponentName").text("");
                        (function(seat, seats) {
                            if (typeof opts.dci === "undefined" || opts.dci == seat.player) {
                                dataset.getPlayer(seat.player, function(err, player) {
                                    console.log("getPlayer: " + seat.player);

                                    // find opponents
                                    var opponents = seats.filter(function(o) {
                                            return o.player != player.dci;
                                        })
                                    
                                    // load all the opponents                                        
                                    async.parallel(opponents.map(function(o) {
                                        return function(callback) {
                                            dataset.getPlayer(o.player, function(err, player) {
                                                callback(null, player.first + " " + player.last)
                                            })
                                        }
                                    }), function(err, results) {
                                        var opponentName = results.join(", ");
                                        var notify = {
                                            dci: seat.player,
                                            seatNumber: table.number,
                                            playerName: player.first + " " + player.last,
                                            roundName: roundName,
                                            opponentName: opponentName,
                                            eventName: event.name
                                        }

                                        if (typeof opts.sessionId == 'undefined') {
                                            sessions.getSessions(seat.player, function(sessionId) {
                                                sessions.seating(sessionId, notify);
                                            })
                                        }
                                        else {
                                            sessions.seating(opts.sessionId, notify);
                                        }

                                    })

                                })
                            }
                        })(seat, table.seats);
                    }
                })(table);
            }
        };

        for (var i = 0, len = event.rounds.length; i < len; i++) {
            var round = event.rounds[i];
            if ("" + round.number == "" + opts.roundNumber) {
                process(round);
            }
        }
    }

    // find the latest update for a player
    var notifyPlayer = function(dci, sessionId) {
        dataset.getEvents({
            "players.dci": dci
        }, function(err, events) {
            if (events.length > 0) {
                for (var index in events) {
                    var roundNumber = Math.max.apply(null, events[index].rounds.map(function(r) {
                        return r.number;
                    }));
                    notifyRound(events[index], {
                        roundNumber: roundNumber,
                        dci: dci,
                        sessionId: sessionId
                    });
                }
            }
            else {
                if (typeof sessionId == 'undefined') {
                    sessions.getSessions(dci, function(sessionId) {
                        sessions.waiting(sessionId);
                    })
                }
                else {
                    sessions.waiting(sessionId);
                }
            }
        });
    }

    dataset.on('event_deleted', function(event) {
        for (var index in event.players) {
            var player = event.players[index];
            notifyPlayer(player.dci);
        }
    });

    dataset.on('event_created', function(result) {
        var array = result.rounds.map(function(r) {
            return parseInt(r.number);
        });
        var roundNumber = Math.max.apply(null, array);
        notifyRound(result, {
            roundNumber: roundNumber
        });
    })

    io.on('connection', function(socket) {
        console.log('connected');

        var sessionId = socket.handshake.session.sessionId;
        sessions.attachSession(sessionId, socket, 'player', function(sessionId) {
            sessions.login(sessionId);
        });

        socket.on('disconnect', function() {
            console.log("[" + sessionId + ']: disconnect');
            sessions.disconnect(sessionId, socket.id);
        });

        socket.on('logout', function(data) {
            console.log("[" + sessionId + ']: logout');
            var session = sessions.getSession(sessionId);
            if (session) {
                delete session.resend.player;
                delete session.dci;
            }
            sessions.login(sessionId);
        })

        socket.on('heartbeat', function() {
            console.log("[" + sessionId + ']: heartbeat');
            sessions.touch(sessionId);
        })

        socket.on('login', function(data) {
            var dci = data.dci;
            console.log("[" + sessionId + ']: login dci:' + dci);

            var session = sessions.getSession(sessionId);
            session.dci = dci;

            dataset.getPlayer(dci, function(err, player) {
                if (player == null) {
                    console.log("[" + sessionId + "]: player not found: " + dci);
                    sessions.waiting(sessionId);
                }
                else {
                    console.log("[" + sessionId + "]: notify player: " + dci);
                    notifyPlayer(dci, sessionId);
                }
            });
        })
    });

    return {}
};
