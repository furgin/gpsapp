module.exports = function(dataset, sessions) {
    var express = require('express');
    var router = express.Router();
    var Charlatan = require('charlatan');

    var updateSessionId = function(req, mode) {
        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
        var sessionId = req.session.sessionId;
        var session = {};
        if (typeof sessionId == 'undefined') {
            sessionId = guid();
            console.log("new session: " + sessionId)
            session = sessions.createSession(sessionId,mode);
        }
        else {
            console.log("found sessionId");
            session = sessions.getSession(sessionId);
            if (session) {
                console.log("[" + sessionId + "]: found session: " + session.dci)
            }
            else {
                sessionId = guid();
                console.log("new session: " + sessionId)
                session = sessions.createSession(sessionId);
            }
        }
        return sessionId;
    }

    router.get('/', function(req, res) {
        req.session.sessionId = updateSessionId(req,'player');
        res.render('client/app');
    });

    router.get('/admin', function(req, res) {
        req.session.sessionId = updateSessionId(req,'admin');
        res.render('admin/app');
    });

    router.get('/admin/generate', function(req, res) {
        function random(top) {
            return Math.floor(Math.random() * top)
        }
        var count = req.query.count || 10;
        var players = [];
        for (var i = 0; i < count; i++) {
            var dci = "";
            for (var j = 0; j < 9; j++) {
                dci += ("" + random(10))
            }
            players.push({
                dci: dci,
                first: Charlatan.Name.firstName(),
                last: Charlatan.Name.lastName(),
                middle: 'M',
                country: 'AU'
            })
        }
        var games = [];
        var players2 = players.slice();
        var table = 1;
        while (players2.length >= 2) {
            var player1 = players2.pop();
            var player2 = players2.pop();
            games.push({
                table: table,
                player1: player1.dci,
                player2: player2.dci
            });
            table += 1;
        }
        console.log(games);

        var randomCharacters = function(len) {
            var allowed = "0123456789abcdef"
            var str = "";
            for (var i = 0; i < len; i++) {
                str += allowed.charAt(random(allowed.length));
            }
            return str;
        }

        var eventId = randomCharacters(8) + "-" + randomCharacters(4) + "-" + randomCharacters(4) + "-" + randomCharacters(4) + "-" + randomCharacters(12);

        res.render('admin/generate', {
            eventGuid: eventId,
            players: players,
            games: games,
            eventId: random(5000)
        });
    });

    return router;
};
