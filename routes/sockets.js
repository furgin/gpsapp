module.exports = function(io, dataset, sessions) {

    var async = require('async');
    var xml2js = require('xml2js');
    var werfile = require('./werfile')();

    var updateEvent = function(sessionId, wer, event, loadedEvent, callback) {
        var log = wer.log();
        var players = wer.players();
        var roundNumber = "" + wer.round();

        console.log("roundNumber: " + roundNumber);

        async.parallel(players.map(function(player) {
            return function(callback) {
                dataset.getPlayer(player.dci, function(err, result) {
                    if (result == null) {
                        dataset.createPlayer(player, function(err, result) {
                            console.log("player created: " + player.dci);
                            callback(null, result);
                        });
                    }
                    else {
                        dataset.updatePlayer(player.dci, player, function() {
                            callback(null, result);
                        });
                    }
                })
            }
        }), function(err, results) {
            
            sessions.fileUploaded(sessionId, "Players created...")
            
            if (loadedEvent == null) {
                var tables = wer.tables();
                var round = {
                    number: roundNumber,
                    tables: tables
                }
                event.rounds = [round];

                event.players = players.map(function(p) {
                    return {
                        dci: p.dci,
                        name: p.first + " " + p.last
                    }
                });
                dataset.createEvent(event, function(err, event) {
                    sessions.fileUploaded(sessionId, "Event created...")
                    // event has been created
                    callback();
                })
            }
            else {

                var tables = wer.tables();
                var round = {
                    number: roundNumber,
                    tables: tables
                }

                var rounds = loadedEvent.rounds.filter(function(r) {
                    return r.number != roundNumber;
                });
                rounds.push(round);
                loadedEvent.rounds = rounds;

                dataset.updateEvent(loadedEvent.eventId, loadedEvent, function(err, updatedEvent) {
                    sessions.fileUploaded(sessionId, "Event updated...")                    
                    callback();
                })
            }
        });
    }

    var eventsPage = function(sessionId) {
        dataset.getEvents({},function(err, all) {
            sessions.adminEvents(sessionId, all.map(function(e) {
                return { 
                    eventId: e.eventId,
                    name: e.name,
                    players: e.players,
                    rounds: e.rounds
                }
            }));
        });
    }

    io.on('connection', function(socket) {

        var sessionId = socket.handshake.session.sessionId;
        sessions.attachSession(sessionId, socket, 'admin', function(sessionId){
            sessions.admin(sessionId);
        });

        socket.on('disconnect', function() {
            console.log("[" + sessionId + ']: disconnect');
            sessions.disconnect(sessionId,socket.id);
        });

        socket.on('event_delete', function(data) {
            console.log("delete event: " + data.eventId);
            var eventId = data.eventId === "undefined" ? undefined : data.eventId;
            dataset.deleteEvent(eventId, function(err, event) {
                console.log("event deleted: " + data.eventId);
                eventsPage(sessionId);
            });
        });

        socket.on('upload', function(data) {
            // TODO check admin
            console.log("upload file: " + data.file);
            sessions.fileUploaded(sessionId, "File Uploaded...");
            var wer = werfile.load(data.text, function(wer) {
                var werEvent = wer.event();
                console.log("event loaded: " + werEvent.eventId)
                dataset.getEvent(werEvent.eventId, function(err, loadedEvent) {
                    updateEvent(sessionId, wer, werEvent, loadedEvent, function() {
                        eventsPage(sessionId);
                    });
                });
            });
        });

        socket.on('admin', function(data) {
            var email = data.email;
            var password = data.password;
            console.log('connected as admin ' + email)

            var session = sessions.getSession(sessionId);
            session.admin = {
                email: email
            };

            dataset.getUser(email, password, function(err, user) {
                if (user != null) {
                    console.log('connect as admin: email=' + user.email)
                    eventsPage(sessionId);
                }
                else {
                    console.log("user " + user + " not logged in");
                }
            })
        });

        socket.on('logout', function(data) {
            console.log("[" + sessionId + ']: logout');
            var session = sessions.getSession(sessionId);
            if (session) {
                delete session.resend.admin
                delete session.admin;
            }
            sessions.admin(sessionId);
        })

    });

    return {}
};
