var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

function genuuid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

var session = require('express-session')({
    genid: function(req) {
        return genuuid() // use UUIDs for session IDs 
    },
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
});
var bodyParser = require('body-parser');

function createApp(routes, port) {
    var application = express();

    application.set('views', path.join(__dirname, 'views'));
    application.set('view engine', 'jade');

    //application.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    application.use(logger('dev'));
    application.use(bodyParser.json());
    application.use(bodyParser.urlencoded({
        extended: false
    }));
    application.use(cookieParser());
    application.use(session)

    application.use(express.static(path.join(__dirname, 'public')));
    application.use('/', routes);

    application.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if (application.get('env') === 'development') {
        application.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('client/error', {
                message: err.message,
                error: err
            });
        });
    }
    application.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('client/error', {
            message: err.message,
            error: {}
        });
    });
    application.set('port', port);

    return application;
}

var io = require('socket.io')();

var sessions = require('./routes/sessions')();
var dataset = require('./routes/dataset')();
var players = require('./routes/players')(io, dataset, sessions);
var sockets = require('./routes/sockets')(io, dataset, sessions);

var port = normalizePort(process.env.PORT || '3000');
var app = createApp(require('./routes/index')(dataset, sessions), port);
var http = require('http');
var server = http.createServer(app);
io.listen(server.listen(port));

var sharedsession = require("express-socket.io-session");
io.use(sharedsession(session, {
    autoSave:true
}));

function address(server) {
    var addressObject = server.address();
    if (addressObject) {
        var address = addressObject.address == "::" ? "localhost" : addressObject.address;
        return 'http://' + address + ':' + addressObject.port + '/';
    }
    else {
        return 'unknown';
    }
}

server.on('error', onError);
console.log('player' + ' server: ' + address(server));

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}