$(function() {
    var socket = io();

    function readTextFile(file, callback) {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function() {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    var allText = rawFile.responseText;
                    callback(allText);
                }
            }
        }
        rawFile.send(null);
    }

    //
    // SOCKET EVENTS
    //

    socket.on('reload', function() {
        location.reload();
    })

    socket.on('page-admin-login', function(data) {
        $(".page").hide();
        $("#loginform-page").show();
        $("#loginformemail").focus();
    });
    
    socket.on('file-uploaded', function(data) {
        $("#file-upload-dialog").find("p").html(data.message);
    })

    socket.on("page-events", function(data) {
        $(".page").hide();
        var ul = $("<ul/>");
        if (data.events) {
            for (var i = 0, len = data.events.length; i < len; i++) {
                var info = "<span class='rouds item'>Rounds: " + data.events[i].rounds.length + "</span>";
                info += "<span class='players item'>Players: " + data.events[i].players.length + "</span>"
                var buttons = "<a href='#' data-id='" + data.events[i].eventId + "' class='action-delete button'/>";
                
                var html = "<li><div class='event'>";
                html += "<span class='name'>" + data.events[i].name + "</span>";
                html += "<span class='info'>" + info + "</span>";
                html += "<span class='icons small'>" + buttons + "</span>";
                html += "</div></li>";
                ul.append(html);
            }
        }
        $("#events-page").find(".content").children().remove();
        $("#events-page").find(".content").append(ul);
        $("#events-page").find(".content").find(".action-delete").click(function(e) {
            var eventId = $(this).attr('data-id');
            console.log("delete event: " + eventId);
            socket.emit("event_delete", {
                eventId: eventId
            });
        })
        $("#events-page").show();
    })

    //
    // EVENTS PAGE
    //

    $("#page-events-upload-file").click(function() {
        $("#uploadfile-page").show();
    })

    $("#page-events-logout").click(function() {
        socket.emit('logout');
    })

    //
    // UPLOAD FILE PAGE
    //
    $("#uploadfileformsubmit").click(function(e) {
        e.preventDefault();
        
        $("#uploadfile-page").hide();
        $("#file-upload-dialog").show();
        
        var file = $("#uploadfileformfile").val();
        console.log("upload file: " + file)
        var input = document.getElementById('uploadfileformfile');
        var reader = new FileReader();
        reader.onload = function() {
            var text = reader.result;
            socket.emit('upload', {
                file: $(input).val(),
                text: text
            })
        };
        reader.readAsText(input.files[0]);
    });

    $("#uploadfileformcancel").click(function(e) {
        $("#uploadfile-page").hide();
        e.preventDefault();
    });


    //
    // LOGIN PAGE
    //

    $("#loginformemail").focus().keypress(function(e) {
        if (e.keyCode == 13) {
            $("#loginformpassword").focus();
        }
    })

    $("#loginformpassword").keypress(function(e) {
        if (e.keyCode == 13) {
            $("#loginformsubmit").click();
        }
    })

    $("#loginformsubmit").click(function(e) {
        e.preventDefault();
        var email = $("#loginformemail").val();
        var password = $("#loginformpassword").val();
        console.log("connect with " + email + " " + password)
        socket.emit("admin", {
            email: email,
            password: password
        });
    });
})