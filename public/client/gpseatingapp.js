/* global $ io location */

$(function() {

    console.log("starting gps local");

    var socket = io();

    setInterval(function() {
        socket.emit('heartbeat');
    }, 90 * 1000);

    var showSeating = function(seating) {
        $(".page").hide();
        $("#eventName").text(seating.eventName);
        $("#roundName").text(seating.roundName);
        $("#playerName").text(seating.playerName);
        $("#seatNumber").text(seating.seatNumber);
        $("#opponentName").text(seating.opponentName);
        $("#seating-page").show();
    };

    socket.on('reload', function(data) {
        location.reload();
    });

    socket.on('page-login', function(data) {
        $(".page").hide();
        $("#login-page").show();
        $("#loginformdci").focus();
    });

    socket.on('page-noevent', function(data) {
        showSeating({
            roundName: "",
            playerName: "Waiting for Event",
            seatNumber: "n/a",
            opponentName: ""
        });
    });

    socket.on('page-seating', function(data) {
        showSeating(data);
    });

    var add = function(str) {
        if ($("#loginformdci").html() == "&nbsp;") {
            $("#loginformdci").html("");
        }
        var text = $("#loginformdci").text();
        text += str;
        $("#loginformdci").text(text);
    };

    $("#n1").click(function() {
        add("1");
    });
    $("#n2").click(function() {
        add("2");
    });
    $("#n3").click(function() {
        add("3");
    });
    $("#n4").click(function() {
        add("4");
    });
    $("#n5").click(function() {
        add("5");
    });
    $("#n6").click(function() {
        add("6");
    });
    $("#n7").click(function() {
        add("7");
    });
    $("#n8").click(function() {
        add("8");
    });
    $("#n9").click(function() {
        add("9");
    });
    $("#n0").click(function() {
        add("0");
    });
    $("#nclear").click(function() {
        $("#loginformdci").html("&nbsp;");
    });
    $("#ndel").click(function() {
        var text = $("#loginformdci").text();
        if (text.length > 0) {
            $("#loginformdci").text(text.substring(0, text.length - 1));
        }
        if ($("#loginformdci").text() == 0) {
            $("#loginformdci").html("&nbsp;");
        }
    });

    $("#loginformsubmit").click(function(e) {
        var dci = $("#loginformdci").text();
        console.log("login with dci:" + dci);
        socket.emit('login', {
            dci: dci
        });
        showSeating({
            roundName: "",
            playerName: "Connecting",
            seatNumber: "n/a",
            opponentName: ""
        });
        e.preventDefault();
    });

    $("#header-return").click(function(e) {
        e.preventDefault();
        socket.emit('logout');
    });

})